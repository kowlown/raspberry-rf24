#include "receiver.h"

using namespace std;

RF24 radio(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ);

const uint8_t data_pipe[6] = "00001";
const int max_payload_size = 32;
char receive_payload[max_payload_size + 1];
const uint64_t deviceID = 0xE8E8F0F0E9LL;
uint8_t counter = 1;

struct WeatherDate {
    float humidity;
    float temperature;
    float pression;
} data;

//Initialization
void setup() {
    radio.begin();
    radio.setRetries(15,15);
//    radio.setDataRate(RF24_250KBPS);
    radio.setAutoAck(1);
    radio.setPALevel(RF24_PA_MAX); // Allow optional ack payloads
//    radio.enableDynamicPayloads();
    radio.openReadingPipe(1,data_pipe);
    radio.powerUp();
    radio.printDetails(); // Dump the configuration of the rf unit for debugging
    radio.startListening();

}

//We want to listen and wait from a message if have one or return an empty string.
//It's synchronous.
char* listen() {

    cout << "listen" << endl;
    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    bool timeout = false;
    string payload;

    // While nothing is received
    while (!radio.available() && !timeout) {
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        auto duration = chrono::duration_cast<chrono::seconds>(end - start);
        usleep(5000); //Sleep for 5 ms
        // If waited longer than 1 seconds, indicate timeout and exit while loop
        if (duration.count() > 1) {
            timeout = true;
        }
    }

    if (timeout) {                                             // Describe the results
        cout << "Failed, response timed out." << endl;
    } else {
        radio.read( &data, sizeof(data));
        printf("\nRadio is ON | ");
        //radio.writeAckPayload(counter,counter , sizeof(counter));
        printf(" Temp = %f " ,data.temperature);
    }

    return '\0';
}

