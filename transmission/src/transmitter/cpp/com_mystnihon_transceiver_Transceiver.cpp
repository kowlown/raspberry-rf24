#include "receiver.h"
#include "com_mystnihon_transceiver_Transceiver.h"

//Receiver receiver;

JNIEXPORT void JNICALL Java_com_mystnihon_transceiver_Transceiver_initialize  (JNIEnv *env, jobject obj) {
    std::cout << "JNI setup" << std::endl;
    setup();
}

JNIEXPORT jstring JNICALL Java_com_mystnihon_transceiver_Transceiver_readMessage (JNIEnv *env, jobject obj) {
    std::cout << "JNI listen" << std::endl;
    char* payload = listen();
    return env->NewStringUTF(payload);
}

JNIEXPORT void JNICALL Java_com_mystnihon_transceiver_Transceiver_sendMessage
  (JNIEnv *env, jobject obj, jstring message) {

}

JNIEXPORT void JNICALL Java_com_mystnihon_transceiver_Transceiver_nativeRelease
  (JNIEnv *env, jobject obj) {

}
