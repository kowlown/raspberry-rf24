package com.mystnihon.transceiver;

import java.util.Objects;

/**
 * This class manage the asynchronous of the the message received with event listener.
 */
class Transceiver implements Runnable {
    private static boolean loaded;

    static {
        try {
            System.loadLibrary("transmitter");
            loaded = true;
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Error\n" + e.toString());
            loaded = false;
        }
    }

    private final MessageListener messageListener;
    private volatile boolean run = false;

    Transceiver(MessageListener messageListener) {
        Objects.requireNonNull(messageListener);
        this.messageListener = messageListener;
    }

    @Override
    public void run() {
        if (!loaded) return;
        run = true;
        initialize();
        //We want to periodically check if we have a message and send a callback if so.
        while (run) {
//            System.out.println("Java:Start read");
            String message = readMessage();
//            System.out.println("Java:End read=> message:" + message);
            messageReceived(message);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                System.err.println("Java:Er:" + e.getMessage());
            }
        }
    }

    //region Native methods

    public native void initialize();

    //TODO Future implementation
    public native void sendMessage(String message);

    private native String readMessage();

    private native void nativeRelease();

    //endregion

    void release() {
        if (!loaded) return;
        run = false;
        nativeRelease();
    }

    private void messageReceived(String message) {
        //messageListener is always not null. Test is not needed.
        messageListener.onMessageReceived(message);
    }
}
