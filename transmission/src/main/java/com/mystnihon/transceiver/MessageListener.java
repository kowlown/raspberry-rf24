package com.mystnihon.transceiver;

public interface MessageListener {
    void onMessageReceived(String message);
}
