package com.mystnihon.station;

import com.mystnihon.transceiver.RadioCommunication;

public class Application {


    public static void main(String[] args) {

        RadioCommunication radioCommunication = new RadioCommunication();

//        radioCommunication.setMessageListener(message -> System.out.println("Java:" + message));
        radioCommunication.start();
    }

}
